// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import prisma from "@/lib/prisma";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { equation } = req.body;
  const result = await prisma.calculationHistory.create({
    data: {
      equation: equation,
    },
  });
  res.json(result);
}
