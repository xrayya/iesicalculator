import { useState, useEffect } from "react";
import { calculatePostfix } from "@/utils/postfixCalculator";
import { infixToPostfix } from "@/utils/infixConverter";
import prisma from "@/lib/prisma";
import CalculatorScreen from "@/components/CalculatorScreen";
import { buttonList } from "@/components/buttonList";
import CalculatorButton from "@/components/CalculatorButton";

export default function Home() {
  const [fixDisplayText, setFixDisplayText] = useState<string>("");
  const [inputList, setInputList] = useState<Array<number | string>>([]);
  const [newInputValue, setNewInputValue] = useState<string>("");
  const [result, setResult] = useState<number | null>(null);

  const handleValueButtonOnClick = (
    event: React.MouseEvent<HTMLInputElement>,
  ) => {
    if (typeof inputList[inputList.length - 1] === "number") {
      setInputList([]);
    }

    if (
      (event.target as HTMLInputElement).value === "%" &&
      newInputValue !== ""
    ) {
      setNewInputValue((prev) => (parseFloat(prev) * 0.01).toString());

      return;
    }

    setNewInputValue((prev) => prev + (event.target as HTMLInputElement).value);
  };

  const handleOperatorButtonOnClick = (
    event: React.MouseEvent<HTMLInputElement>,
  ) => {
    if (newInputValue === "") {
      return;
    }

    const operator = (event.target as HTMLInputElement).value;

    setFixDisplayText((prev) => prev + newInputValue + operator);

    setInputList((list) => {
      let newList = [...list];
      newList.push(parseFloat(newInputValue));
      newList.push(operator);
      setNewInputValue("");
      return newList;
    });
  };

  const handleControlButtonOnClick = async (
    event: React.MouseEvent<HTMLInputElement>,
  ) => {
    if ((event.target as HTMLInputElement).value === "=") {
      if (
        typeof inputList[inputList.length - 1] === "string" &&
        newInputValue === ""
      ) {
        setInputList((list) => {
          let newList = [...list];
          newList.pop();
          return newList;
        });

        setFixDisplayText((prev) => prev.substring(0, prev.length - 1));
        return;
      }

      if (typeof inputList[inputList.length - 1] !== "number") {
        setFixDisplayText((prev) => prev + newInputValue);

        setInputList((list) => {
          let newList = [...list];
          newList.push(parseFloat(newInputValue));
          setResult(calculatePostfix(infixToPostfix(newList)));
          return newList;
        });

        // try {
        //   const body = {
        //     equation: `${fixDisplayText}${newInputValue}=${result}`,
        //   };
        //   await fetch("/api/history", {
        //     method: "POST",
        //     headers: { "Content-Type": "application/json" },
        //     body: JSON.stringify(body),
        //   });
        // } catch (error) {
        //   console.log(error);
        // }

        // await prisma.calculationHistory.create({
        //   data: {
        //     equation: `${fixDisplayText}${newInputValue}=${result}`,
        //   },
        // });

        // setInputList([result]);
        // setFixDisplayText(result.toString());
        // setNewInputValue("");
      }
    }
  };

  useEffect(() => {
    if (result !== null) {
      const push = async () => {
        try {
          const body = {
            equation: `${fixDisplayText}=${result}`,
          };
          await fetch("/api/history", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body),
          });
        } catch (error) {
          console.log(error);
        }
      };

      push();
      setInputList([result]);
      setFixDisplayText(result.toString());
      setNewInputValue("");
    }

    setResult(null);
  }, [result]);

  return (
    <div className="p-4">
      <h2>Calculator</h2>
      <div className="rounded-lg p-2 border-2 border-gray-600 max-w-screen-sm">
        <CalculatorScreen displayString={fixDisplayText + newInputValue} />
        <div className="mt-4 grid grid-cols-4 gap-2">
          {buttonList.map((button, index) => (
            <CalculatorButton
              key={index}
              label={button.label}
              type={button.type}
              onClick={
                button.type === "value"
                  ? handleValueButtonOnClick
                  : button.type === "operator"
                  ? handleOperatorButtonOnClick
                  : handleControlButtonOnClick
              }
            />
          ))}
        </div>
      </div>
    </div>
  );
}
