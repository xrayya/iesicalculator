import { screenProps } from "@/Calculator.types";
import React from "react";

const CalculatorScreen: React.FC<screenProps> = ({ displayString }) => {
  return (
    <input
      value={displayString.toString()}
      readOnly
      className="p-4 w-full flex items-center bg-gray-300 rounded-lg"
    />
  );
};

export default CalculatorScreen;
