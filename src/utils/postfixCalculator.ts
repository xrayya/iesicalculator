import { Stack } from "datastructures-js";

export function calculatePostfix(inputs: Array<number | String>) {
  let stack = new Stack<number>();
  inputs.forEach((input) => {
    if (typeof input === "number") {
      stack.push(input);
    } else {
      switch (input) {
        case "+":
          stack.push(stack.pop() + stack.pop());
          break;
        case "-":
          stack.push(stack.pop() - stack.pop());
          break;
        case "×":
          stack.push(stack.pop() * stack.pop());
          break;
        case "÷":
          stack.push(stack.pop() / stack.pop());
          break;
      }
    }
  });

  return stack.pop() ?? 0;
}
