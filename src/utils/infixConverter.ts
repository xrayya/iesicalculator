import { Stack } from "datastructures-js";

export function infixToPostfix(
  inputs: Array<number | string>,
): Array<number | String> {
  console.log(inputs)
  let stack = new Stack<number | string>();
  let result = new Stack<number | string>();
  inputs.forEach((input) => {
    // console.log("input", input)
    // console.log("stack", stack)
    switch (input) {
      case ")":
        while (!stack.isEmpty()) {
          if (stack.peek() !== "(") {
            stack.pop();
            break;
          }

          result.push(stack.pop()!);
        }
        break;
      case "(":
      case "^":
        stack.push(input);
        break;
      case "×":
      case "÷":
        if (stack.isEmpty()) {
          stack.push(input);
          break;
        }

        while (!stack.isEmpty()) {
          if (
            stack.peek() === "(" ||
            stack.peek() === "+" ||
            stack.peek() === "-"
          )
            break;

          result.push(stack.pop()!);
        }

        stack.push(input);
        break;

      case "+":
      case "-":
        if (stack.isEmpty()) {
          stack.push(input);
          break;
        }

        while (!stack.isEmpty()) {
          if (stack.peek() === "(") {
            break;
          } else {
            result.push(stack.pop()!);
          }
        }

        stack.push(input);
        break;

      default:
        result.push(input);
        break;
    }
  });
  while (!stack.isEmpty()) {
    result.push(stack.pop()!);
  }

  console.log(result.toArray())
  return result.toArray();
}
